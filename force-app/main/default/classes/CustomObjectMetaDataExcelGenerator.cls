/**
 * Class Name :  CustomObjectMetaDataExcelGenerator
 * Description :  used to generate an excel file and send to email that contains information all custom objects in you salesforce
 * Author : Dhana Prasad
 */
public with sharing class CustomObjectMetaDataExcelGenerator {

    /**
     * Method Name : makecsvwithobjectandfield
     */
    public static void makecsvwithobjectandfield(){
        map<string, SObjectType> objs = schema.getGlobalDescribe();
        string header = 'Object Name, Field, Label \n'; // headerd of csv file
        string finalstr = header ;
        for(string key: objs.keySet()){
            //if condtion to handle namespace objects
            if(key.endsWithIgnoreCase('__c')){ //__c indicates custom objects
                map<string, string> fieldList = new map<string, string>();
                if(key != null){
                    map<string,SObjectField> fList = schema.getGlobalDescribe().get(key).getDescribe().fields.getMap();
                    for(string str: fList.keySet()){
                        fieldList.put(str, fList.get(str).getDescribe().getLabel());                
                    }
                    string recordString;
                    for(string objmap : fieldList.keyset()){
                        recordString = key+','+objmap+','+fieldList.get(objmap)+'\n';                        
                        finalstr = finalstr +recordString;
                    }
                    finalstr = finalstr +recordString+'\n';
                }else{
                    return;
                }
            }
        }
        Messaging.EmailFileAttachment csvAttc = new          Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'Report.csv'; // name of file for csv
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new list<string> {'dhana.prasad@mtxb2b.com'}; //email id where attachment needs to be send 
        String subject ='Custom Objects information of your Salesforce Org';
        email.setSubject(subject);
        email.setToAddresses( toAddresses );
        email.setPlainTextBody('Hi,\n Here is the information of all custom objects in following csv fle');
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
}
